from turtle import *

def move():

    sety(0)
    setheading(0)

    color("black")
    
    penup()
    forward(250)
    #left(90)
    pendown()

def silly_face():

    #Face
    fillcolor('yellow')
    begin_fill()
    circle(100)
    end_fill()
    penup()

    #Left eye
    goto(-50,120)
    pendown()
    fillcolor("black")
    begin_fill()
    circle(10)
    end_fill()
    penup()

    #Right eye
    goto(50,120)
    pendown()
    fillcolor("black")
    begin_fill()
    circle(10)
    end_fill()
    penup()

    #Mouth
    goto(-40, 85)
    pendown()
    right(90)
    circle(40, 180)
    penup()

    #tongue
    goto(-10, 48)
    down()
    right(190)
    fillcolor('red')
    begin_fill()
    circle(10, 185)
    end_fill()
    hideturtle()

    penup()

def smiley():

    begin_fill()
    color("yellow")
    
    circle(90)
    
    end_fill()
    
    color("black")
    penup()
    left(90)
    forward(130)
    left(90)
    forward(40)
    pendown()
    
    begin_fill()
    circle(10)
    end_fill()
    
    penup()
    right(180)
    forward(70)
    right(90)
    forward(10)
    pendown()
    
    begin_fill()
    circle(10)
    end_fill()
    
    penup()
    forward(60)
    right(90)
    forward(80)
    left(90)
    pendown()
    circle(50, 180)
    
    penup()
        
def open_smiley():

    begin_fill()
    color("yellow")
    
    circle(90)
    
    end_fill()
    
    color("black")
    penup()
    left(90)
    forward(130)
    left(90)
    forward(40)
    pendown()
    
    begin_fill()
    circle(10)
    end_fill()
    
    penup()
    right(180)
    forward(70)
    right(90)
    forward(10)
    pendown()
    
    begin_fill()
    circle(10)
    end_fill()
    
    penup()
    forward(60)
    right(90)
    forward(80)
    left(90)
    pendown()
    
    begin_fill()
    
    circle(50, 180)
    
    #right(90)
    #forward(80)
    
    end_fill()
    
    penup() 
    
def frown():

    begin_fill()
    color("yellow")
    
    circle(90)
    
    end_fill()
    
    color("black")
    penup()
    left(90)
    forward(130)
    left(90)
    forward(40)
    pendown()
    
    begin_fill()
    circle(10)
    end_fill()
    
    penup()
    right(180)
    forward(70)
    right(90)
    forward(10)
    pendown()
    
    begin_fill()
    circle(10)
    end_fill()
    
    penup()
    forward(85)
    left(90)
    forward(15)
    right(90)
    
    left(180)
    
    pendown()
    circle(47, 180)
    
    penup()

def open_frown():

    begin_fill()
    color("yellow")
    
    circle(90)
    
    end_fill()
    
    color("black")
    penup()
    left(90)
    forward(130)
    left(90)
    forward(40)
    pendown()
    
    begin_fill()
    circle(10)
    end_fill()
    
    penup()
    right(180)
    forward(70)
    right(90)
    forward(10)
    pendown()
    
    begin_fill()
    circle(10)
    end_fill()
    
    penup()
    forward(85)
    left(90)
    forward(15)
    right(90)
    
    left(180)
    
    pendown()
    
    begin_fill()
    
    circle(47, 180)
    
    end_fill()
    
    penup()
      
def winky():

    begin_fill()
    color("yellow")
    
    circle(90)
    
    end_fill()
    
    color("black")
    penup()
    left(90)
    forward(130)
    left(90)
    forward(40)
    pendown()
    
    begin_fill()
    circle(10)
    end_fill()
    
    penup()
    right(180)
    forward(70)
    right(90)
    forward(10)
    pendown()
    
    circle(10, 180)
    
    penup()
    right(180)
    forward(60)
    right(90)
    forward(100)
    left(90)
    
    pendown()
    
    begin_fill()
    
    circle(50, 180)
    
    end_fill()
    
    penup()

print("This program has the following emoji:")
print("1. Smiley face")
print("2. Open Smiley face")
print("3. Frowney face")
print("4. Open Frowney face")
print("5. Winking face")
print("6. Sticking out tongue")
print("Enter 0 to stop.")

while True:

    prompt = input("Which emoji should be next? ")
    
    if prompt == "0":
        break
        
    if prompt.lower() == "1":
        smiley()
        move()
        
    if prompt.lower() == "2":
        open_smiley()
        move()
        
    if prompt.lower() == "3":
        frown()
        move()
        
    if prompt.lower() == "4":
        open_frown()
        move()
    
    if prompt.lower() == "5":
        winky()
        move()
        
    if prompt.lower() == "6":
        silly_face()
        move()
        
    #else:
        #print("Invalid input, try again.")
        
#speed(0)
#silly_face()

done()
